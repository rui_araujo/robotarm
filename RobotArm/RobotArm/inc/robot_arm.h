#ifndef SETPOSITION_H_
#define ROBOT_ARM_H_

#define HEADER_VALUE		0xFF

#define MAX_SPEED_MASK		1023

#define PING_INSTRUCTION 			0x1
#define READ_INSTRUCTION 			0x2
#define WRITE_INSTRUCTION 			0x3
#define SYNCWRITE_INSTRUCTION 		0x83

#define BROADCAST_ID			254

#define TOTAL_SERVOS		6

#define MAX_PACKET_SIZE		(0xFF+2+1+1) // max length + 2 bytes for the header +1 for id + 1 for checksum#define UART3_RX_PORT		0
#define UART3_RX_PIN		1

typedef enum statusCode {
	OK, INVALID_ID, TIMEOUT, STATUS_ERROR
} status_t;

#define ERROR_POSITION			4

#define SERVO1_DEFAULT			0
#define SERVO2_DEFAULT			385
#define SERVO3_DEFAULT			205
#define SERVO4_DEFAULT			2222
#define SERVO5_DEFAULT			512
#define SERVO6_DEFAULT			829

#define MAX_ID					252

#define ID_ADDRESS				3
#define GOAL_POSITION_ADDRESS	30 //Length:2#define MOVING_SPEED_ADDRESS	32 //Length:2#define TORQUE_ADDRESS			34 //Length:2#define POSITION_ADDRESS		36 //Length:2#define SPEED_ADDRESS			38 //Length:2#define LOAD_ADDRESS			40 //Length:2#define VOLTAGE_ADDRESS			42#define TEMPERATURE_ADDRESS		43enum robotServoId {
	SERVO1 = 1, SERVO2, SERVO3, SERVO4, SERVO5, SERVO6,
};
void RobotArmInit();
status_t readFromAddress(uint8_t id, uint8_t address, uint8_t length);
uint8_t * getReadData();
status_t setIdBroadcast(uint8_t newId);
status_t setId(uint8_t oldId, uint8_t newId, uint8_t *packetError);
status_t ping(uint8_t id, uint8_t *packetError);
status_t setPositionArm(uint8_t id, uint16_t goal, uint16_t speed);
status_t setDesiredPositionArm(uint8_t id, uint16_t goal, uint16_t speed);
status_t broadcastDesiredPositionArm();
status_t readPosition(uint8_t id);
status_t readSpeed(uint8_t id);
uint16_t getSpeed(uint8_t id);
uint16_t getPosition(uint8_t id);
status_t readVoltage(uint8_t id);
uint16_t getVoltage(uint8_t id);
status_t readLoad(uint8_t id);
uint16_t getLoad(uint8_t id);
status_t readTemperature(uint8_t id);
uint16_t getTemperature(uint8_t id);
status_t getError(uint8_t id);
#endif /* ROBOT_ARM_H_ */
