/*
 * pinout.h
 *
 *  Created on: Jul 30, 2014
 *      Author: raraujo
 */

#ifndef PINOUT_H_
#define PINOUT_H_


#define UART0_RX_PORT		0
#define UART0_RX_PIN		3

#define UART0_TX_PORT		0
#define UART0_TX_PIN		2

#define UART0_CTS_PORT		2
#define UART0_CTS_PIN		12

#define UART0_RTS_PORT		2
#define UART0_RTS_PIN		11

#define UART3_RX_PORT		0
#define UART3_RX_PIN		1

#define UART3_TX_PORT		0
#define UART3_TX_PIN		0

#define LED_PORT		0
#define LED_PIN			22

#endif /* PINOUT_H_ */
