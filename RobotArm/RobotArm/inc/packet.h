/*
 * packet.h
 *
 *  Created on: Jun 18, 2014
 *      Author: raraujo
 */

#ifndef PACKET_H_
#define PACKET_H_

#include <stdint.h>

typedef struct
{

	uint32_t header;
	uint8_t  instr;
	uint8_t  id;
	uint16_t param;

} __attribute__((__packed__)) packet_t;


typedef struct
{

	uint8_t header[2];
	uint8_t id;
	uint8_t length;
	uint8_t error;
	uint8_t param[2];
	uint8_t cks;

} __attribute__((__packed__)) receive_t;



#endif /* PACKET_H_ */
