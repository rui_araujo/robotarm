#include <string.h>
#include "chip.h"
#include "robot_arm.h"
#include "utils.h"
#include "uart.h"
#include "xprintf.h"
#include "error.h"
#include "sensors.h"
#include "pinout.h"
#include <stdbool.h>
#include <ctype.h>

#define SOFTWARE_VERSION "0.1"

#define DEFAULT_BAUDRATE_UART0			(4000000)
#define DEFAULT_BAUDRATE_UART3			(1000000)

#define TX_BUFFER_SIZE_BITS		(12)
#define TX_BUFFER_SIZE			(1<<TX_BUFFER_SIZE_BITS)

#define RX_BUFFER_SIZE_BITS		(12)
#define RX_BUFFER_SIZE			(1<<RX_BUFFER_SIZE_BITS)

//Transmit buffer that will be used on the rest of the system
RINGBUFF_T uart0TX;
RINGBUFF_T uart0RX;

RINGBUFF_T uart3RX;

volatile bool clearToSend = false;

char uart0TxBuffer[TX_BUFFER_SIZE];
char uart0RxBuffer[RX_BUFFER_SIZE];
char uart3RxBuffer[RX_BUFFER_SIZE];

//length of "2012-03-02 12:00:00"
#define TIME_DATE_COM_SIZE (20)

#define UART_COMMAND_LINE_MAX_LENGTH  	128

// *****************************************************************************

unsigned char commandLine[UART_COMMAND_LINE_MAX_LENGTH];
uint32_t commandLinePointer;
uint32_t enableUARTecho;	 // 0-no cmd echo, 1-only cmd reply, 2-all visible

// *****************************************************************************
#define UARTReturn()	   xputc('\n')

/* The rate at which data is sent to the queue, specified in milliseconds. */


/* UART transmit-only interrupt handler for ring buffers */
static inline void Chip_UART_TXIntHandlerFlowControl() {
	uint8_t ch;

	/* Fill FIFO until full or until TX ring buffer is empty */
	if (Chip_GPIO_ReadPortBit(LPC_GPIO, UART0_CTS_PORT, UART0_CTS_PIN) == 0) { // no rts stop signal
		clearToSend = true;
		while ((Chip_UART_ReadLineStatus(LPC_UART0) & UART_LSR_THRE) != 0 && RingBuffer_Pop(&uart0TX, &ch)) {
			Chip_UART_SendByte(LPC_UART0, ch);
		}
	} else {
		clearToSend = false;
	}
}

void UARTRestartTX() {
	/* Don't let UART transmit ring buffer change in the UART IRQ handler */
	Chip_UART_IntDisable(LPC_UART0, UART_IER_THREINT);
	Chip_UART_TXIntHandlerFlowControl();
	/* Enable UART transmit interrupt */
	Chip_UART_IntEnable(LPC_UART0, UART_IER_THREINT);
}

void UART0WriteChar(char pcBuffer) {

	uint32_t ret;
	do {
		/* Don't let UART transmit ring buffer change in the UART IRQ handler */
		Chip_UART_IntDisable(LPC_UART0, UART_IER_THREINT);

		/* Move as much data as possible into transmit ring buffer */
		ret = RingBuffer_Insert(&uart0TX, &pcBuffer);
		Chip_UART_TXIntHandlerFlowControl();

		/* Enable UART transmit interrupt */
		Chip_UART_IntEnable(LPC_UART0, UART_IER_THREINT);
	} while (ret == 0);
}

void UART0_IRQHandler(void) {

	uint32_t interruptIdentifier;
	/* Handle transmit interrupt if enabled */
	if (LPC_UART0->IER & UART_IER_THREINT) {
		Chip_UART_TXIntHandlerFlowControl();

		/* Disable transmit interrupt if the ring buffer is empty */
		if (RingBuffer_IsEmpty(&uart0TX)) {
			Chip_UART_IntDisable(LPC_UART0, UART_IER_THREINT);
		}
	}
	while (((interruptIdentifier = LPC_UART0->IIR) & 0x01) == 0) {
		if (interruptIdentifier & UART_IIR_INTID_RDA) {
			/* Handle receive interrupt */
			Chip_UART_RXIntHandlerRB(LPC_UART0, &uart0RX);
			if (RingBuffer_GetFree(&uart0RX) < CTS_BUFFER_THRESHOLD) {
				//Please stop!
				Chip_GPIO_SetPinOutHigh(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
			} else {
				//Ready to do business!
				Chip_GPIO_SetPinOutLow(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
			}
		}
	}
}

void UART3_IRQHandler(void) {
	uint32_t interruptIdentifier;
	while (((interruptIdentifier = LPC_UART3->IIR) & 0x01) == 0) {
		if (interruptIdentifier & UART_IIR_INTID_RDA) {
			/* Handle receive interrupt */
			Chip_UART_RXIntHandlerRB(LPC_UART3, &uart3RX);
		}
	}
}

void UARTsInit() {
	memset(commandLine, 0, UART_COMMAND_LINE_MAX_LENGTH);
	commandLinePointer = 0;
	enableUARTecho = 1;
	xdev_out(UART0WriteChar);
	Chip_GPIO_SetPinOutLow(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, UART0_CTS_PORT, UART0_CTS_PIN);
	Chip_UART_Init(LPC_UART0);
	Chip_UART_ConfigData(LPC_UART0, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
	Chip_UART_SetupFIFOS(LPC_UART0, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2));
	Chip_UART_IntEnable(LPC_UART0, UART_IER_RBRINT);
	Chip_UART_SetBaudFDR(LPC_UART0, DEFAULT_BAUDRATE_UART0);
	Chip_UART_TXEnable(LPC_UART0);
	NVIC_EnableIRQ(UART0_IRQn);
	Chip_UART_Init(LPC_UART3);
	Chip_UART_ConfigData(LPC_UART3, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT);
	Chip_UART_SetupFIFOS(LPC_UART3, UART_FCR_FIFO_EN | UART_FCR_TRG_LEV0);
	Chip_UART_IntEnable(LPC_UART3, UART_IER_RBRINT);
	Chip_UART_SetBaudFDR(LPC_UART3, DEFAULT_BAUDRATE_UART3);
	Chip_UART_TXEnable(LPC_UART3);
	RingBuffer_Init(&uart0TX, uart0TxBuffer, 1, TX_BUFFER_SIZE);
	RingBuffer_Init(&uart0RX, uart0RxBuffer, 1, RX_BUFFER_SIZE);
	RingBuffer_Init(&uart3RX, uart3RxBuffer, 1, RX_BUFFER_SIZE);
	NVIC_EnableIRQ(UART3_IRQn);
}

// *****************************************************************************

// *****************************************************************************
void UARTShowVersion(void) {
	xputs(
			"\nRobotArmLPC1759, V" SOFTWARE_VERSION " " __DATE__ ", " __TIME__ "\n");
}

// *****************************************************************************
static void UARTShowUsage(void) {

	UARTShowVersion();

	UARTReturn();
	xputs("Supported Commands:\n");
	UARTReturn();

	xputs(" ?Ca                 - ping servo with id a\n");
	UARTReturn();

	xputs(" !Ia=b               - set servo id a to id b\n");
	xputs(" !I=b                - set servo id to b\n");
	UARTReturn();

	xputs(
			" !D+b,p              - enable sensors streaming, ??D to show options\n");
	xputs(
			" !D-[b]              - disable sensors streaming, ??D to show options\n");
	xputs(" ?Db                 - get sensor readouts according to bitmap b\n");
	xputs(" ??D                 - bitmap b options\n");
	UARTReturn();

	xputs(" ?Ex                 - print verbose explanation of the error x\n");
	xputs(" ??E                 - print error table\n");
	UARTReturn();

	xputs(" ?L[1-6]             - get servo load\n");
	xputs(" ?LA                 - get all servos load\n");
	UARTReturn();

	xputs(" ?Mid,address,len    - read servos memory\n");

	xputs(" !P[1-6],p,[s]       - set servo position (speed: optional)\n");
	xputs(" !PA[1-6],p,[s][|]   - set all servos position\n");
	xputs(" !PR                 - reset servo position to safe value\n");
	xputs(" ?P[1-6]             - get servo position\n");
	xputs(" ?PA                 - get all servos positions\n");
	UARTReturn();

	xputs(" ?S[1-6]             - get servo velocity\n");
	xputs(" ?SA                 - get all servos velocities\n");
	UARTReturn();

	xputs(" ?T[1-6]             - get servo temperature\n");
	xputs(" ?TA                 - get all servos temperatures\n");
	UARTReturn();

	xputs(" ?V[1-6]             - get servo voltage\n");
	xputs(" ?VA                 - get all servos voltages\n");
	UARTReturn();

	xputs(" !U=x                - set baud rate to x\n");
	xputs(" !U[0,1]             - UART echo mode (none, all)\n");
	UARTReturn();

	xputs(" ??                  - display (this) help\n");
	UARTReturn();
}

// *****************************************************************************
static uint32_t parseUInt32(unsigned char **c) {
	uint32_t ul = 0;
	while (((**c) >= '0') && ((**c) <= '9')) {
		ul = 10 * ul;
		ul += ((**c) - '0');
		(*(c))++;
	}
	return (ul);
}

// *****************************************************************************
// * ** parseGetCommand ** */
// *****************************************************************************
static void UARTParseGetCommand(void) {

	switch (commandLine[1]) {

	case '?':
		if (commandLine[2] == 'e' || commandLine[2] == 'E') {
			printErrorTable();
			break;
		}
		if (((commandLine[2]) == 'd') || ((commandLine[2]) == 'D')) {
			PrintSensorOptions();
			break;
		}
		UARTShowUsage();
		break;
	case 'c':
	case 'C': {
		unsigned char *c = commandLine + 2;
		if (!isdigit(*c)) {
			xputs("Id must must be an integer\n");
			break;
		}
		uint32_t id = parseUInt32(&c);
		if (id > MAX_ID) {
			xprintf("Id must must be an integer between 0 and %d\n",
			MAX_ID);
			break;
		}
		uint8_t error;
		if (ping(id, &error)) {
			xprintf("-E%d %d\n", id, error);
			break;
		}
		xprintf("-C%d\n", id);
		break;
	}
	case 'd':
	case 'D': {
		unsigned char *c = commandLine + 2;
		getSensorsOutput(parseUInt32(&c));
		break;
	}
	case 'e':
	case 'E': {
		unsigned char *c = commandLine + 2;
		printError(parseUInt32(&c));
		break;
	}
	case 'l':
	case 'L': {
		unsigned char *c = commandLine + 2;
		if (*c == 'a' || *c == 'A') {
			allLoadsReport();
			break;
		} else if (*c >= ('0' + SERVO1) && *c <= ('0' + SERVO6)) {
			uint8_t currentId = *c - '0';
			if (readLoad(currentId)) {
				xprintf("-E%d %d\n", currentId, getError(currentId));
				break;
			}
			xprintf("-L%d %d\n", currentId, getLoad(currentId));
			break;
		}
	}

	case 'm':
	case 'M': {
		unsigned char *c = commandLine + 2;
		if (!isdigit(*c)) {
			xputs("Id must must be an integer\n");
			break;
		}
		uint32_t id = parseUInt32(&c);
		if (id > MAX_ID) {
			xprintf("Id must be an integer between 0 and %d\n",
			MAX_ID);
			break;
		}
		if (*c != ',') {
			xputs("Get: parsing error\n");
			break;
		}
		c++;
		uint32_t address = parseUInt32(&c);
		if (address > 0xFF) {
			xputs("Address must be an integer between 0 and 0xFF\n");
			break;
		}
		if (*c != ',') {
			xputs("Get: parsing error\n");
			break;
		}
		c++;
		uint32_t length = parseUInt32(&c);
		if (length > 0xFF) {
			xputs("Length must be an integer between 0 and 0xFF\n");
			break;
		}
		if (address + length > 0x100) {
			xputs("Length must not overrun the memory\n");
			break;
		}
		if (readFromAddress(id, address, length)) {
			xprintf("-E%d %d\n", id, TIMEOUT_ERROR);
			break;
		}
		xprintf("-M%d", id);
		uint8_t * dataRead = getReadData();
		for (int i = 0; i < length; ++i) {
			xprintf(" %X", (uint32_t) dataRead[i] & 0xFF);
		}
		xputc('\n');
		break;
	}
	case 'p':
	case 'P': {
		unsigned char *c = commandLine + 2;
		if (*c == 'a' || *c == 'A') {
			allPositionsReport();
			break;
		} else if (*c >= ('0' + SERVO1) && *c <= ('0' + SERVO6)) {
			uint8_t currentId = *c - '0';
			if (readPosition(currentId)) {
				xprintf("-E%d %d\n", currentId, getError(currentId));
				break;
			}
			xprintf("-P%d %d\n", currentId, getPosition(currentId));
			break;
		}
	}
	case 's':
	case 'S': {
		unsigned char *c = commandLine + 2;
		if (*c == 'a' || *c == 'A') {
			allVelocitiesReport();
			break;
		} else if (*c >= ('0' + SERVO1) && *c <= ('0' + SERVO6)) {
			uint8_t currentId = *c - '0';
			if (readSpeed(currentId)) {
				xprintf("-E%d %d\n", currentId, getError(currentId));
				break;
			}
			xprintf("-S%d %d\n", currentId, getSpeed(currentId));
			break;
		}
	}
	case 't':
	case 'T': {
		unsigned char *c = commandLine + 2;
		if (*c == 'a' || *c == 'A') {
			allTemperaturesReport();
			break;
		} else if (*c >= ('0' + SERVO1) && *c <= ('0' + SERVO6)) {
			uint8_t currentId = *c - '0';
			if (readTemperature(currentId)) {
				xprintf("-E%d %d\n", currentId, getError(currentId));
				break;
			}
			xprintf("-T%d %d\n", currentId, getTemperature(currentId));
			break;
		}
	}
	case 'v':
	case 'V': {
		unsigned char *c = commandLine + 2;
		if (*c == 'a' || *c == 'A') {
			allVoltagesReport();
			break;
		} else if (*c >= ('0' + SERVO1) && *c <= ('0' + SERVO6)) {
			uint8_t currentId = *c - '0';
			if (readVoltage(currentId)) {
				xprintf("-E%d %d\n", currentId, getError(currentId));
				break;
			}
			xprintf("-V%d %d\n", currentId, getVoltage(currentId));
			break;
		}
	}
	default:
		xputs("Get: parsing error\n");
	}
}

// *****************************************************************************
// * ** parseSetCommand ** */
// *****************************************************************************
static void UARTParseSetCommand(void) {
	switch (commandLine[1]) {

	case 'D':
	case 'd': {
		unsigned char *c = commandLine + 2;
		uint8_t flag = 0;
		if (*c == '+') {
			flag = ENABLE;
		} else if (*c == '-') {
			flag = DISABLE;
		} else {
			break;
		}
		c++;
		if (!isdigit(*c)) {
			if (!flag && (commandLinePointer == 3)) {
				enableSensors(0xFF, flag, 0);
				return;
			}
			break;
		}
		uint32_t mask = parseUInt32(&c);
		if (*c == ',') {
			c++;
		} else {
			if (flag) {
				break; //second argument only mandatory when enabling.
			}
		}
		uint32_t period = 1;
		if (flag) {
			period = parseUInt32(&c);
		}
		enableSensors(mask, flag, period);
		return;
		return;
	}
	case 'i':
	case 'I': {
		unsigned char *c = commandLine + 2;
		uint8_t error;
		if (*c == '=') {
			//Unknown current id
			c++;
			if (!isdigit(*c)) {
				xputs("Id must be an integer\n");
				return;
			}
			uint32_t id = parseUInt32(&c);
			if (id > MAX_ID) {
				xprintf("Id must be an integer between 0 and %d\n", MAX_ID);
				return;
			}
			setIdBroadcast(id);
			timerDelayMs(100);
			if (ping(id, &error)) {
				xprintf("-E%d %d\n", id, TIMEOUT_ERROR);
				return;
			}
			xprintf("-I%d\n", id);
			return;
		} else if (!isdigit(*c)) {
			xputs("Id must be an integer\n");
			return;
		}
		uint32_t oldId = parseUInt32(&c);
		if (oldId > MAX_ID) {
			xprintf("Id must be an integer between 0 and %d\n", MAX_ID);
			return;
		}
		if (*c != '=') {
			break;
		}
		c++;
		uint32_t newId = parseUInt32(&c);
		if (newId > MAX_ID) {
			xprintf("Id must be an integer between 0 and %d\n", MAX_ID);
			return;
		}
		if (setId(oldId, newId, &error)) {
			xprintf("-E%d %d\n", oldId, error);
			return;
		}
		if (ping(newId, &error)) {
			xprintf("-E%d %d\n", oldId, error);
			return;
		}
		xprintf("-I%d\n", newId);
		return;
	}
	case 'p':
	case 'P': {
		unsigned char *c = commandLine + 2;
		if (*c == 'r' || *c == 'R') {
			xputs("-PR\n");
			setPositionArm(SERVO1, SERVO1_DEFAULT, 0);
			setPositionArm(SERVO2, SERVO2_DEFAULT, 0);
			setPositionArm(SERVO3, SERVO3_DEFAULT, 0);
			setPositionArm(SERVO4, SERVO4_DEFAULT, 0);
			setPositionArm(SERVO5, SERVO5_DEFAULT, 0);
			setPositionArm(SERVO6, SERVO6_DEFAULT, 0);
			break;
		} else if (*c == 'a' || *c == 'A') {
			c++;
			bool noErrors = true;
			while (noErrors) {
				if (*c < ('0' + SERVO1) || *c > ('0' + SERVO6)) {
					xputs("Set: wrong id\n");
					noErrors = false;
					continue;
				}
				uint8_t currentId = *c - '0';
				c++;
				if (*c == ',') {
					c++;
				} else {
					xputs("Set: parsing error\n");
					noErrors = false;
					continue;
				}
				uint16_t goal = parseUInt32(&c);
				uint16_t speed = 0;
				if (*c == ',') {
					c++;
					speed = parseUInt32(&c);
				}
				setDesiredPositionArm(currentId, goal, speed);
				if (*c == '|') {
					c++;
					if (*c == '\0') { //We have reached the end
						break;
					}
				} else { //We have reached the end
					break;
				}
			}
			if (noErrors) {
				broadcastDesiredPositionArm();
				xputs("-PA\n");
			}
			break;
		} else if (*c >= ('0' + SERVO1) && *c <= ('0' + SERVO6)) {
			uint8_t currentId = *c - '0';
			c++;
			if (*c == ',') {
				c++;
			} else {
				xputs("Set: parsing error\n");
				break;
			}
			uint16_t goal = parseUInt32(&c);
			uint16_t speed = 0;
			if (*c == ',') {
				c++;
				speed = parseUInt32(&c);
			}
			if (setPositionArm(currentId, goal, speed)) {
				xprintf("-E%d %d\n", currentId, getError(currentId));
				break;
			}
			xprintf("-P%d\n", currentId);
			break;
		}
		xputs("Set: parsing error\n");
		break;
	}
	case 'U':
	case 'u': {
		unsigned char *c;
		long baudRate;
		c = commandLine + 2;
		if (((*c) >= '0') && ((*c) <= '2')) {
			enableUARTecho = ((*c) - '0');
			break;
		}
		if (*c == '=') {
			c++;
		} else {
			xputs("Set: parsing error\n");
			break;
		}
		baudRate = parseUInt32(&c);
		while ((LPC_UART0->LSR & UART_LSR_TEMT) == 0) {
		};		   // wait for UART to finish data transfer
		xprintf("-U=%d\n", baudRate);
		timerDelayMs(100);
		if (Chip_UART_SetBaudFDR(LPC_UART0, baudRate) == 0) {
			xprintf("Failed to switch Baud Rate to %d Baud!\n", baudRate);
		}
		break;
	}

	default:
		xputs("Set: parsing error\n");
	}
}

// *****************************************************************************
// * ** parseRS232CommandLine ** */
// *****************************************************************************
static void parseRS232CommandLine(void) {

	switch (commandLine[0]) {
	case '?':
		UARTParseGetCommand();
		break;
	case '!':
		UARTParseSetCommand();
		break;
	default:
		xputs("?\n");
	}
}

// *****************************************************************************
// * ** RS232ParseNewChar ** */
// *****************************************************************************
void UART0ParseNewChar() {
	unsigned char newChar;
	RingBuffer_Pop(&uart0RX, &newChar);
	switch (newChar) {
	case 8:			// backspace
		if (commandLinePointer > 0) {
			commandLinePointer--;
			if (enableUARTecho) {
				xprintf("%c %c", 8, 8);
			}
		}
		break;

	case 10:
	case 13:
		if (enableUARTecho) {
			UARTReturn();
		}
		if (commandLinePointer > 0) {
			commandLine[commandLinePointer] = 0;
			parseRS232CommandLine();
			commandLinePointer = 0;
		}
		break;

	default:
		if (newChar & 0x80) {
			return; //only accept ASCII
		}
		if (commandLinePointer < UART_COMMAND_LINE_MAX_LENGTH - 1) {
			if (enableUARTecho) {
				xputc(newChar);	  		   	// echo to indicate char arrived
			}
			commandLine[commandLinePointer++] = newChar;
		} else {
			commandLinePointer = 0;
			commandLine[commandLinePointer++] = newChar;
		}
	}  // end of switch

}  // end of rs232ParseNewChar

