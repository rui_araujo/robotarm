/*
 * error.c
 *
 *  Created on: Jun 30, 2014
 *      Author: raraujo
 */
#include "error.h"
#include "xprintf.h"
const char *errorVerbose[] = { "Input Voltage Error", "Angle Limit Error", "Overheating Error", "Range Error", "Checksum Error", "Overload Error",
		"Instruction Error", "Timeout Error" };

void printErrorTable(void) {
	xputs("Bitlist for the error code:\n");
	xputs(" Bit Dec-Value Description\n");
	xputs(" 0   1         The applied voltage is out of the range of operating voltage\n");
	xputs(" 1   2         Goal Position is out of range\n");
	xputs(" 2   4         Internal temperature of Dynamixel is out of the range\n");
	xputs(" 3   8         Command is out of the range for use\n");
	xputs(" 4   16        Checksum error\n");
	xputs(" 5   32        Current load cannot be controlled by the set Torque\n");
	xputs(" 6   64        Undefined instruction\n");
	xputs(" 7   128       Servo did not reply to command\n\n");
}

void printError(uint8_t error) {
	for (int i = 0; i < 8; ++i) {
		if (error & _BIT(i)) {
			xprintf("%s\n", errorVerbose[i]);
		}
	}
}
