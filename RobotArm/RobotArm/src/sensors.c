/*
 * sensors.c
 *
 *  Created on: Apr 10, 2014
 *      Author: raraujo
 */

#include "chip.h"
#include "sensors.h"
#include "xprintf.h"
#include "robot_arm.h"
#include <stdbool.h>

uint32_t sensorsEnabledCounter;
struct sensorTimer sensorsTimers[MAX_SENSORS];
volatile uint8_t sensorRefreshRequested;
struct sensorTimer * enabledSensors[MAX_SENSORS];

void PrintSensorOptions(void) {
	xputs("Bitlist for available sensors:\n");
	xputs(" Bit Dec-Value Name      # Values  Description\n");
	xputs(" 0   1         ALL POSITIONS    6  all servos' positions\n");
	xputs(" 1   2         ALL_VOLTAGES     6  all servos' voltages\n");
	xputs(" 2   4         ALL_SPEED        6  all servos' velocities\n");
	xputs(" 3   8         ALL_LOADS        6  all servos' loads\n");
	xputs(" 4   16        ALL_TEMPERATURES 6  all servos' temperatures\n\n");
}

void allPositionsReport() {
	xputs("-PA");
	for (int i = SERVO1; i <= SERVO6; ++i) {
		if (readPosition(i)) {
			xprintf(" -%d", getError(i));
		} else {
			xprintf(" %d", getPosition(i));
		}
	}
	xputc('\n');
}

void allVelocitiesReport() {
	xputs("-SA");
	for (int i = SERVO1; i <= SERVO6; ++i) {
		if (readSpeed(i)) {
			xprintf(" -%d", getError(i));
		} else {
			xprintf(" %d", getSpeed(i));
		}
	}
	xputc('\n');

}

void allLoadsReport() {
	xputs("-LA");
	for (int i = SERVO1; i <= SERVO6; ++i) {
		if (readLoad(i)) {
			xprintf(" -%d", getError(i));
		} else {
			xprintf(" %d", getLoad(i));
		}
	}
	xputc('\n');
}

void allTemperaturesReport() {
	xputs("-TA");
	for (int i = SERVO1; i <= SERVO6; ++i) {
		if (readTemperature(i)) {
			xprintf(" -%d", getError(i));
		} else {
			xprintf(" %d", getTemperature(i));
		}
	}
	xputc('\n');
}

void allVoltagesReport() {
	xputs("-VA");
	for (int i = SERVO1; i <= SERVO6; ++i) {
		if (readVoltage(i)) {
			xprintf(" -%d", getError(i));
		} else {
			xprintf(" %d", getVoltage(i));
		}
	}
	xputc('\n');
}

void sensorsInit(void) {
	sensorRefreshRequested = 0;
	sensorsEnabledCounter = 0;
	for (int i = 0; i < MAX_SENSORS; ++i) {
		enabledSensors[i] = NULL;
		sensorsTimers[i].triggered = false;
		sensorsTimers[i].reload = 0;
		sensorsTimers[i].counter = 0;
		sensorsTimers[i].position = -1;
		switch (i) {
		case 0:
			sensorsTimers[i].refresh = allPositionsReport;
			break;
		case 1:
			sensorsTimers[i].refresh = allVoltagesReport;
			break;
		case 2:
			sensorsTimers[i].refresh = allVelocitiesReport;
			break;
		case 3:
			sensorsTimers[i].refresh = allLoadsReport;
			break;
		case 4:
			sensorsTimers[i].refresh = allTemperaturesReport;
			break;
		default:
			sensorsTimers[i].refresh = NULL;
			break;
		}
	}
	uint32_t load = Chip_Clock_GetSystemClockRate() / 1000 - 1;
	if (load > 0xFFFFFF) {
		load = 0xFFFFFF;
	}
	SysTick->LOAD = load;
	SysTick->CTRL |= 0x7;	//enable the Systick
}

void enableSensors(uint8_t mask, uint8_t flag, uint32_t period) {
	for (int i = 0; i < MAX_SENSORS; ++i) {
		if (mask & (1 << i)) {
			enableSensor(i, flag, period);
		}
	}
}

void enableSensor(uint8_t sensorId, uint8_t flag, uint32_t period) {
	if (sensorId >= MAX_SENSORS) {
		return;
	}
	if (sensorsTimers[sensorId].refresh == NULL) {
		return;
	}
	SysTick->CTRL &= ~0x1;	//disable the Systick
	if (flag) {
		if (sensorsTimers[sensorId].position == -1) {
			sensorsTimers[sensorId].counter = period;
			sensorsTimers[sensorId].reload = period;
			enabledSensors[sensorsEnabledCounter++] = &sensorsTimers[sensorId];
			sensorsTimers[sensorId].position = sensorsEnabledCounter - 1;
		} else {
			sensorsTimers[sensorId].reload = period;	//Update the period
		}
	} else {
		if (sensorsTimers[sensorId].position != -1) {
			//if removing the last one, no need to iterate or do anything besides reducing the counter
			if (sensorsTimers[sensorId].position != sensorsEnabledCounter - 1) {
				for (int i = sensorsTimers[sensorId].position; i < sensorsEnabledCounter; ++i) {
					enabledSensors[i] = enabledSensors[i + 1];
				}
			}
			sensorsTimers[sensorId].position = -1;
			sensorsTimers[sensorId].triggered = 0;
			sensorsEnabledCounter--;
		}
	}
	SysTick->CTRL |= 0x1;	//enable the Systick
}

void getSensorsOutput(uint8_t mask) {
	for (int i = 0; i < MAX_SENSORS; ++i) {
		if (mask & (1 << i)) {
			if (sensorsTimers[i].refresh != NULL) {
				sensorsTimers[i].refresh();
			}
		}
	}
}

/**
 * The Systick handler is used for a lot more tasks than sensor timing.
 * It also provides a timer for decaying for the motor velocity, motor control
 * and second timer used for the LED blinking and Retina event rate.
 */
volatile uint8_t toggleLed0 = 0;
void SysTick_Handler(void) {
	static uint16_t second_timer = 0;
	if (++second_timer >= 1000) {
		second_timer = 0;
		toggleLed0 = 1;
	}
	sensorRefreshRequested = 1;
	for (int i = 0; i < sensorsEnabledCounter; ++i) {
		if (--enabledSensors[i]->counter == 0) {
			enabledSensors[i]->counter = enabledSensors[i]->reload;
			enabledSensors[i]->triggered = 1;
		}
	}
}

