#include <cr_section_macros.h>
#include "chip.h"
#include "uart.h"
#include "sensors.h"
#include "robot_arm.h"
#include "pinout.h"

int main(void) {
	RobotArmInit();
	sensorsInit();
	UARTsInit();
	UARTShowVersion();
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, LED_PORT, LED_PIN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_PORT, LED_PIN);
	while (1) {
		if (RingBuffer_GetCount(&uart0RX)) {
			UART0ParseNewChar();
			if (RingBuffer_GetFree(&uart0RX) < CTS_BUFFER_THRESHOLD) {
				//Please stop!
				Chip_GPIO_SetPinOutHigh(LPC_GPIO, UART0_RTS_PORT,
						UART0_RTS_PIN);
			} else {
				//Ready to do business!
				Chip_GPIO_SetPinOutLow(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
			}
		}
		if (sensorRefreshRequested) {
			sensorRefreshRequested = 0;
			for (int i = 0; i < sensorsEnabledCounter; ++i) {
				if (enabledSensors[i]->triggered) {
					enabledSensors[i]->refresh();
					enabledSensors[i]->triggered = 0;
				}
			}
		}
		if (Chip_GPIO_ReadPortBit(LPC_GPIO, UART0_CTS_PORT, UART0_CTS_PIN) == 0) { // no rts stop signal
			if ( !clearToSend && !RingBuffer_IsEmpty(&uart0TX)){
				UARTRestartTX();
			}
		}
		if (toggleLed0) {
			toggleLed0 = 0;
			Chip_GPIO_SetPinToggle(LPC_GPIO, LED_PORT, LED_PIN);
		}
	}
	return 0;
}
