#include "chip.h"
#include "packet.h"
#include "robot_arm.h"
#include "uart.h"
#include "error.h"
#include <string.h>

const uint16_t servosLimits[][2] = { { 0, 4095 }, { 385, 2515 }, { 205, 3059 },
		{ 1014, 3430 }, { 0, 1023 }, { 190, 829 } };

#define TIMEOUT_VALUE 60000 //random big number#define DEFAULT_SPEED		(50)//http://support.robotis.com/en/techsupport_eng.htm#product/dynamixel/communication/dxl_packet.htmstruct dynamixelServo {
	uint8_t id;
	uint8_t currentVoltage;
	uint8_t currentTemperature;
	uint16_t desiredPosition;
	uint16_t desiredSpeed;
	uint16_t currentPosition;
	uint16_t currentSpeed;
	uint16_t currentLoad;
	uint8_t error;
};

struct {
	uint8_t packetBuffer[MAX_PACKET_SIZE];
	struct dynamixelServo servos[TOTAL_SERVOS];
	uint8_t status; /*Bit field */
} robotArm;

void RobotArmInit() {
	memset(&robotArm, 0, sizeof(robotArm));
	for (int i = 0; i < TOTAL_SERVOS; ++i) {
		robotArm.servos[i].id = i + 1;
		robotArm.servos[i].desiredSpeed = DEFAULT_SPEED;
	}
}

static status_t sendPacket(uint32_t sizeTx, uint16_t sizeRx) {
	uint32_t timeout = TIMEOUT_VALUE;
	RingBuffer_Flush(&uart3RX);
	Chip_UART_SendBlocking(LPC_UART3, robotArm.packetBuffer, sizeTx);
	while (RingBuffer_GetCount(&uart3RX) < sizeTx) {
		timeout--;
		if (timeout == 0) {
			return TIMEOUT;
		}
	}
	RingBuffer_PopMult(&uart3RX, robotArm.packetBuffer, sizeTx);
	if (sizeRx > 0) {
		if (sizeRx > MAX_PACKET_SIZE) {
			sizeRx = MAX_PACKET_SIZE; //  discard the rest
		}
		while (RingBuffer_GetCount(&uart3RX) < sizeRx) {
			timeout--;
			if (timeout == 0) {
				return TIMEOUT;
			}
		}
		RingBuffer_PopMult(&uart3RX, robotArm.packetBuffer, sizeRx);
	}
	return OK;
}

static uint8_t calculateCheckSum(uint8_t *data, int numBytes) {
	uint32_t checksumAccumulator = 0;
	for (int i = 2; i < numBytes - 1; ++i) {
		checksumAccumulator += data[i];
	}
	uint8_t checksum = checksumAccumulator & 0xFF;
	return ~checksum;
}

status_t readFromAddress(uint8_t id, uint8_t address, uint8_t length) {
//Header
	robotArm.packetBuffer[0] = HEADER_VALUE;
	robotArm.packetBuffer[1] = HEADER_VALUE;
//ID
	robotArm.packetBuffer[2] = id;
//Length
	robotArm.packetBuffer[3] = 4;
	robotArm.packetBuffer[4] = READ_INSTRUCTION;
	robotArm.packetBuffer[5] = address;

//Read length bytes
	robotArm.packetBuffer[6] = length;
	robotArm.packetBuffer[7] = calculateCheckSum(robotArm.packetBuffer, 8);

	return sendPacket(8, 6 + length);
}

uint8_t * getReadData(){
	return &robotArm.packetBuffer[5];
}

static status_t writeToAddress(uint8_t id, uint8_t address, uint8_t * data,
		uint8_t length) {
	int i = 0;
//Header
	robotArm.packetBuffer[i++] = HEADER_VALUE;
	robotArm.packetBuffer[i++] = HEADER_VALUE;
//ID
	robotArm.packetBuffer[i++] = id;
//Length
	robotArm.packetBuffer[i++] = 2 + length + 1;
	robotArm.packetBuffer[i++] = WRITE_INSTRUCTION;
	robotArm.packetBuffer[i++] = address;

//Write length bytes
	for (int j = 0; j < length; ++j) {
		robotArm.packetBuffer[i++] = data[j];
	}
	uint16_t packetLength = i + 1;
	robotArm.packetBuffer[i++] = calculateCheckSum(robotArm.packetBuffer,
			packetLength);
	if ( id == BROADCAST_ID ){
		return  sendPacket(i, 0);//No status packet return with BROADCAST_ID
	}
	return sendPacket(i, 6);
}
static inline uint16_t limitPosition(uint8_t id, uint16_t goal) {
	if (goal > servosLimits[id - SERVO1][1]) {
		return servosLimits[id - SERVO1][1];
	} else if (goal < servosLimits[id - SERVO1][0]) {
		return servosLimits[id - SERVO1][0];
	}
	return goal;
}

status_t setIdBroadcast(uint8_t newId) {
	return writeToAddress(BROADCAST_ID, ID_ADDRESS, &newId, 1);
}
status_t setId(uint8_t oldId, uint8_t newId, uint8_t *packetError) {
	if (!packetError) {
		*packetError = 0;
	}
	if (writeToAddress(oldId, ID_ADDRESS, &newId, 1)) {
		if (!packetError) {
			*packetError = TIMEOUT_ERROR;
		}
		return TIMEOUT;
	}
	if (!packetError) {
		*packetError = robotArm.packetBuffer[ERROR_POSITION];
	}
	if (robotArm.packetBuffer[ERROR_POSITION] != 0) {
		return STATUS_ERROR;
	}
	return OK;
}
status_t ping(uint8_t id, uint8_t *packetError) {
	//Header
	robotArm.packetBuffer[0] = HEADER_VALUE;
	robotArm.packetBuffer[1] = HEADER_VALUE;
//ID
	robotArm.packetBuffer[2] = id;
//Length
	robotArm.packetBuffer[3] = 2;
	robotArm.packetBuffer[4] = PING_INSTRUCTION;
	robotArm.packetBuffer[5] = calculateCheckSum(robotArm.packetBuffer, 6);
	if (!packetError) {
		*packetError = 0;
	}
	if (sendPacket(6, 6)){
		if (!packetError) {
			*packetError = TIMEOUT_ERROR;
		}
		return TIMEOUT;
	}
	if (!packetError) {
		*packetError = robotArm.packetBuffer[ERROR_POSITION];
	}
	if (robotArm.packetBuffer[ERROR_POSITION] != 0) {
		return STATUS_ERROR;
	}
	return OK;

}
status_t setDesiredPositionArm(uint8_t id, uint16_t goal, uint16_t speed) {
	if (id < SERVO1 || id > SERVO6) {
		return INVALID_ID;
	}
	robotArm.servos[id - SERVO1].desiredPosition = limitPosition(id, goal);
	if (speed != 0) {
		robotArm.servos[id - SERVO1].desiredSpeed = speed;
	}
	return OK;
}
status_t broadcastDesiredPositionArm() {
	int i = 0;
	//Header
	robotArm.packetBuffer[i++] = HEADER_VALUE;
	robotArm.packetBuffer[i++] = HEADER_VALUE;
	//ID
	robotArm.packetBuffer[i++] = BROADCAST_ID;

	robotArm.packetBuffer[i++] = (4 + 1) * TOTAL_SERVOS + 4; //Length L -> data length to be written
	robotArm.packetBuffer[i++] = SYNCWRITE_INSTRUCTION;
	robotArm.packetBuffer[i++] = GOAL_POSITION_ADDRESS;
	robotArm.packetBuffer[i++] = 4;

	for (int j = 0; j < TOTAL_SERVOS; ++j) {
		robotArm.packetBuffer[i++] = j + 1;
		robotArm.packetBuffer[i++] = robotArm.servos[j].desiredPosition & 0xFF;
		robotArm.packetBuffer[i++] = (robotArm.servos[j].desiredPosition >> 8)
				& 0xFF;
		robotArm.packetBuffer[i++] = robotArm.servos[j].desiredSpeed & 0xFF;
		robotArm.packetBuffer[i++] = (robotArm.servos[j].desiredSpeed >> 8)
				& 0xFF;
	}
	uint16_t packetLength = i + 1;
	robotArm.packetBuffer[i++] = calculateCheckSum(robotArm.packetBuffer,
			packetLength);
	if (sendPacket(i, 0)) {
		return TIMEOUT;
	}
	return OK;
}

status_t setPositionArm(uint8_t id, uint16_t goal, uint16_t speed) {
	if (id < SERVO1 || id > SERVO6) {
		return INVALID_ID;
	}
	uint8_t writeBuffer[4];
	if (speed == 0) {
		speed = robotArm.servos[id - SERVO1].desiredSpeed;
	} else {
		speed &= MAX_SPEED_MASK;
		robotArm.servos[id - SERVO1].desiredSpeed = speed;
	}
	goal = limitPosition(id, goal);
	robotArm.servos[id - SERVO1].desiredPosition = goal;
//Position
	writeBuffer[0] = goal & 0xFF;
	writeBuffer[1] = (goal >> 8) & 0xFF;
	writeBuffer[2] = speed & 0xFF;
	writeBuffer[3] = (speed >> 8) & 0xFF;
	robotArm.servos[id - SERVO1].error = 0;
	if (writeToAddress(id, GOAL_POSITION_ADDRESS, writeBuffer, 4)) {
		robotArm.servos[id - SERVO1].error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotArm.servos[id - SERVO1].error = robotArm.packetBuffer[ERROR_POSITION];
	if (robotArm.packetBuffer[ERROR_POSITION] != 0) {
		return STATUS_ERROR;
	}
	return OK;
}

status_t readLoad(uint8_t id) {
	if (id < SERVO1 || id > SERVO6) {
		return INVALID_ID;
	}
	robotArm.servos[id - SERVO1].error = 0;
	if (readFromAddress(id, LOAD_ADDRESS, 2)) {
		robotArm.servos[id - SERVO1].error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotArm.servos[id - SERVO1].error = robotArm.packetBuffer[ERROR_POSITION];
	if (robotArm.packetBuffer[ERROR_POSITION] != 0) {
		return 1;
	}
	robotArm.servos[id - SERVO1].currentLoad = (robotArm.packetBuffer[6] << 8)
			| robotArm.packetBuffer[5];
	return OK;
}

uint16_t getLoad(uint8_t id) {
	return robotArm.servos[id - SERVO1].currentLoad;
}

status_t readPosition(uint8_t id) {
	if (id < SERVO1 || id > SERVO6) {
		return INVALID_ID;
	}
	robotArm.servos[id - SERVO1].error = 0;
	if (readFromAddress(id, POSITION_ADDRESS, 2)) {
		robotArm.servos[id - SERVO1].error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotArm.servos[id - SERVO1].error = robotArm.packetBuffer[ERROR_POSITION];
	if (robotArm.packetBuffer[ERROR_POSITION] != 0) {
		return STATUS_ERROR;
	}
	robotArm.servos[id - SERVO1].currentPosition = (robotArm.packetBuffer[6]
			<< 8) | robotArm.packetBuffer[5];
	return OK;

}

uint16_t getPosition(uint8_t id) {
	return robotArm.servos[id - SERVO1].currentPosition;
}

status_t readSpeed(uint8_t id) {
	if (id < SERVO1 || id > SERVO6) {
		return INVALID_ID;
	}
	robotArm.servos[id - SERVO1].error = 0;
	if (readFromAddress(id, SPEED_ADDRESS, 2)) {
		robotArm.servos[id - SERVO1].error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotArm.servos[id - SERVO1].error = robotArm.packetBuffer[ERROR_POSITION];
	if (robotArm.packetBuffer[ERROR_POSITION] != 0) {
		return 1;
	}
	robotArm.servos[id - SERVO1].currentSpeed = (robotArm.packetBuffer[6] << 8)
			| robotArm.packetBuffer[5];
	return OK;
}

uint16_t getSpeed(uint8_t id) {
	return robotArm.servos[id - SERVO1].currentSpeed;
}

status_t readVoltage(uint8_t id) {
	if (id < SERVO1 || id > SERVO6) {
		return INVALID_ID;
	}
	robotArm.servos[id - SERVO1].error = 0;
	if (readFromAddress(id, VOLTAGE_ADDRESS, 1)) {
		robotArm.servos[id - SERVO1].error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotArm.servos[id - SERVO1].error = robotArm.packetBuffer[ERROR_POSITION];
	if (robotArm.packetBuffer[ERROR_POSITION] != 0) {
		return 1;
	}
	robotArm.servos[id - SERVO1].currentVoltage = robotArm.packetBuffer[5];
	return OK;
}

uint16_t getVoltage(uint8_t id) {
	return robotArm.servos[id - SERVO1].currentVoltage;
}

status_t readTemperature(uint8_t id) {
	if (id < SERVO1 || id > SERVO6) {
		return INVALID_ID;
	}
	robotArm.servos[id - SERVO1].error = 0;
	if (readFromAddress(id, TEMPERATURE_ADDRESS, 1)) {
		robotArm.servos[id - SERVO1].error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotArm.servos[id - SERVO1].error = robotArm.packetBuffer[ERROR_POSITION];
	if (robotArm.packetBuffer[ERROR_POSITION] != 0) {
		return 1;
	}
	robotArm.servos[id - SERVO1].currentTemperature = robotArm.packetBuffer[5];
	return OK;
}

uint16_t getTemperature(uint8_t id) {
	return robotArm.servos[id - SERVO1].currentTemperature;
}

uint8_t getError(uint8_t id) {
	return robotArm.servos[id - SERVO1].error;
}
