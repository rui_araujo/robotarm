function value=checkSliderLimits(value)
%without this function the slider would easily move outside their ranges
%and maybe the programm will stop running. So moving is made "foolproof"

if(value(1)>4095)
value(1)=4095; end
if(value(1)<0)
value(1)=0; end

if(value(2)>2515)
value(2)=2515; end
if(value(2)<385)
value(2)=385; end

if(value(3)>3059)
value(3)=3059; end
if(value(3)<205)
value(3)=205; end

if(value(4)>3430)
value(4)=3430; end
if(value(4)<1014)
value(4)=1014; end

if(value(5)>1023)
value(5)=1023; end
if(value(5)<0)
value(5)=0; end

if(value(6)>829)
value(6)=829; end
if(value(6)<190)
value(6)=190; end
