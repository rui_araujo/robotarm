function varargout = WidowX(varargin)
% WIDOWX MATLAB code for WidowX.fig
%      WIDOWX, by itself, creates a new WIDOWX or raises the existing
%      singleton*.
%
%      H = WIDOWX returns the handle to a new WIDOWX or the handle to
%      the existing singleton*.
%
%      WIDOWX('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WIDOWX.M with the given input arguments.
%
%      WIDOWX('Property','Value',...) creates a new WIDOWX or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before WidowX_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to WidowX_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help WidowX

% Last Modified by GUIDE v2.5 11-Dec-2013 14:28:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @WidowX_OpeningFcn, ...
                   'gui_OutputFcn',  @WidowX_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

    
% --- Executes just before WidowX is made visible.
function WidowX_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to WidowX (see VARARGIN)

% Choose default command line output for WidowX
handles.output = hObject;

delete(instrfindall); %closes all currently open ports
handles.serial.s=serial('/dev/ttyUSB0','Baudrate',4000000,'Parity','none','DataBits',8,'Stopbits',1);
fopen(handles.serial.s);

%handles.joystick.joy=vrjoystick(1); %open joystick/gamepad

handles.timer.t=timer;              %timer to read the information from joystick and to set the changes
handles.timer.t.TimerFcn={@updateUI,handles};
handles.timer.t.StartDelay = 0;
handles.timer.t.Period=0.2;
handles.timer.t.ExecutionMode='fixedRate';
start(handles.timer.t);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes WidowX wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = WidowX_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbQuit.
function pbQuit_Callback(hObject, eventdata, handles)
stop(handles.timer.t);
delete(handles.timer.t);
delete(instrfindall);
close();
% hObject    handle to pbQuit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
 
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',0);


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',385);


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',205);


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',2222); 


% --- Executes on slider movement.
function slider5_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',512);



% --- Executes on slider movement.
function slider6_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function slider6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',829);


% --- Executes on button press in pb_readPosition.
function pb_readPosition_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there can be an asynchronous write because of timer
position=readPosition(handles);
set(handles.edit_currentPositionID1,'String',position(1));
set(handles.edit_currentPositionID2,'String',position(2));
set(handles.edit_currentPositionID3,'String',position(3));
set(handles.edit_currentPositionID4,'String',position(4));
set(handles.edit_currentPositionID5,'String',position(5));
set(handles.edit_currentPositionID6,'String',position(6));
start(handles.timer.t);
% hObject    handle to pb_readPosition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_readVoltage.
function pb_readVoltage_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there would be an asynchronous write because of timer
voltage=readVoltage(handles);
set(handles.edit_currentVoltageID1,'String',(sprintf('%0.1f',voltage(1))));
set(handles.edit_currentVoltageID2,'String',(sprintf('%0.1f',voltage(2))));
set(handles.edit_currentVoltageID3,'String',(sprintf('%0.1f',voltage(3))));
set(handles.edit_currentVoltageID4,'String',(sprintf('%0.1f',voltage(4))));
set(handles.edit_currentVoltageID5,'String',(sprintf('%0.1f',voltage(5))));
set(handles.edit_currentVoltageID6,'String',(sprintf('%0.1f',voltage(6))));
start(handles.timer.t);
% hObject    handle to pb_readVoltage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_id1_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id2_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id3_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id4_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id5_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id6_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edit_id6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentPositionID1_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID2_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID3_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID4_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID5_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID6_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edit_currentPositionID6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentVoltageID1_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentVoltageID1 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentVoltageID1 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentVoltageID1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentVoltageID2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentVoltageID2 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentVoltageID2 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentVoltageID2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentVoltageID3_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentVoltageID3 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentVoltageID3 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentVoltageID3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentVoltageID4_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentVoltageID4 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentVoltageID4 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentVoltageID4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentVoltageID5_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentVoltageID5 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentVoltageID5 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentVoltageID5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentVoltageID6_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentVoltageID6 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentVoltageID6 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentVoltageID6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pb_readTemperature.
function pb_readTemperature_Callback(hObject, eventdata, handles)
% hObject    handle to pb_readTemperature (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
stop(handles.timer.t); pause(0.2);              %if not, there would be an asynchronous write because of timer
temperature=readTemperature(handles);
set(handles.edit_currentTempID1,'String',temperature(1));
set(handles.edit_currentTempID2,'String',temperature(2));
set(handles.edit_currentTempID3,'String',temperature(3));
set(handles.edit_currentTempID4,'String',temperature(4));
set(handles.edit_currentTempID5,'String',temperature(5));
set(handles.edit_currentTempID6,'String',temperature(6));
start(handles.timer.t);


function edit_currentTempID1_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentTempID1 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentTempID1 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentTempID1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentTempID2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentTempID2 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentTempID2 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentTempID2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentTempID3_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentTempID3 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentTempID3 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentTempID3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentTempID4_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentTempID4 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentTempID4 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentTempID4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentTempID5_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentTempID5 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentTempID5 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentTempID5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentTempID6_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentTempID6 as text
%        str2double(get(hObject,'String')) returns contents of edit_currentTempID6 as a double


% --- Executes during object creation, after setting all properties.
function edit_currentTempID6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentTempID6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
