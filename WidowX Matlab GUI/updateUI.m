function []=updateUI(myTimerObj, thisEvent, handles)
setPosition(myTimerObj, thisEvent, handles);
position=readPosition(handles);
set(handles.edit_currentPositionID1,'String',position(1));
set(handles.edit_currentPositionID2,'String',position(2));
set(handles.edit_currentPositionID3,'String',position(3));
set(handles.edit_currentPositionID4,'String',position(4));
set(handles.edit_currentPositionID5,'String',position(5));
set(handles.edit_currentPositionID6,'String',position(6));
temperature=readTemperature(handles);
set(handles.edit_currentTempID1,'String',temperature(1));
set(handles.edit_currentTempID2,'String',temperature(2));
set(handles.edit_currentTempID3,'String',temperature(3));
set(handles.edit_currentTempID4,'String',temperature(4));
set(handles.edit_currentTempID5,'String',temperature(5));
set(handles.edit_currentTempID6,'String',temperature(6));
voltage=readVoltage(handles);
set(handles.edit_currentVoltageID1,'String',(sprintf('%0.1f',voltage(1))));
set(handles.edit_currentVoltageID2,'String',(sprintf('%0.1f',voltage(2))));
set(handles.edit_currentVoltageID3,'String',(sprintf('%0.1f',voltage(3))));
set(handles.edit_currentVoltageID4,'String',(sprintf('%0.1f',voltage(4))));
set(handles.edit_currentVoltageID5,'String',(sprintf('%0.1f',voltage(5))));
set(handles.edit_currentVoltageID6,'String',(sprintf('%0.1f',voltage(6))));