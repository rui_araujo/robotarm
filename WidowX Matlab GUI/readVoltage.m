function [volt]=readVoltage(handles)
fwrite(handles.serial.s,sprintf('?VA\n'),'sync');
fgets(handles.serial.s); %echo
reply = fgets(handles.serial.s);
volt=sscanf(reply, '-VA %d %d %d %d %d %d');
for j=1:6
    if ( volt(j) > 0 )
        volt(j) = volt(j)/10;
    end
end