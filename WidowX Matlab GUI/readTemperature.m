function [temp]=readTemperature(handles)
fwrite(handles.serial.s,sprintf('?TA\n'),'sync');
fgets(handles.serial.s); %echo
reply = fgets(handles.serial.s);
temp = sscanf(reply, '-TA %d %d %d %d %d %d');